# startpage

A simple startpage for everyday use. Features autocomplete for URLs, search history and custom autocompletes. You can find a preview [here](https://t3y.eu/preview/startpage/). Use `/help` for a list of commands.

The default background picture was taken by [Stephen Seeber](https://www.pexels.com/@stywo) and is licensed under the Pexel License
